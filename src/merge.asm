.text
	.globl merge

#
#	$a0 buffer address
#	$a1 buffer length
#
#	|----|----|----|----|		|----|----|----|----|
#	|  2 |  2 |  0 |  4 |  => 	|  4 |  0 |  0 |  4 |
#	|----|----|----|----|		|----|----|----|----|

merge:

check_loop_merge:
bgtz $a1 loop_merge		# continue loop while buffer length is not null
b end

loop_merge:
addiu $a1 $a1 -1    		# loop counter -1 to check length
beqz $a1 end     		# go to end_check if length is 0
#merge begin
lw $t2 ($a0)
lh $t0 ($t2)			# load number from field
addiu $a0 $a0 4			# get next field (!check if exist!)
lw $t3 ($a0)
lh $t1 ($t3)			# load number from field+1
beq $t0 $t1 merge_it
#merge end
b check_loop_merge

merge_it:
sh $zero ($t3)			# set 0 to field+1 as it moved one left
li $t7 2
mul $t4 $t7 $t1
sh $t4 ($t2)			# set number to original value
b check_loop_merge

end:
jr $ra