.text
	.globl place
	
# 	$a0 board address
# 	$a1 board length
#	$a2 field number to place into
#	$a3 number to place
#
#	$v0 == 0 iff place succesfull 
#          else 1
#

place:				
beqz $a1 is_not_empty		# checken ob feld existiert

check_empty:
li $t0 2               		# set number 2
mul $t1 $a2 $t0        		# multiply fieldnumber with 2 => result saved in t1
addu $a0 $a0 $t1       		# get fieldaddress a0 by adding pc+t1
lhu $t2 ($a0)          		# load number from fieldaddress
beqz $t2 is_empty      		# check if number is 0
b is_not_empty         		# if not 0

is_empty:
sh $a3 ($a0)           		# save number a3 into address
li $v0 0
jr $ra

is_not_empty:
li $v0 1
jr $ra
