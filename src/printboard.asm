.data
	.globl printboard
	
topline:
	.asciiz "-----------------------------\n|      |      |      |      |\n"
bottomline:
	.asciiz "\n|      |      |      |      |\n-----------------------------"
wall:
	.asciiz "|"
inbetweenline:
	.asciiz "\n|      |      |      |      |\n-----------------------------\n|      |      |      |      |\n"
newline:
	.asciiz "\n"
space_one:
	.asciiz " "
space_two:
	.asciiz "  "
space_three:
	.asciiz "   "
space_four:
	.asciiz "    "
	
	
#
#a0 Address of the first field off the board
#
#	-----------------------------
#	|      |      |      |      |
#	| 2048 |  128 |    8 | 1024 |
#	|      |      |      |      |
#	-----------------------------
#	|      |      |      |      |
#	| 1024 |   64 |    4 |    8 |
#	|      |      |      |      |
#	-----------------------------
#	|      |      |      |      |
#	|  512 |   32 |  512 |  128 |
#	|      |      |      |      |
#	-----------------------------
#	|      |      |      |      |
#	|  256 |   16 | 2048 | 1024 |
#	|      |      |      |      |
#	-----------------------------
#
.text
printboard:

# save integers
lhu $t0 ($a0)
addiu $a0 $a0 2
lhu $t1 ($a0)
addiu $a0 $a0 2
lhu $t2 ($a0)
addiu $a0 $a0 2
lhu $t3 ($a0)
addiu $a0 $a0 2
lhu $t4 ($a0)
addiu $a0 $a0 2
lhu $t5 ($a0)
addiu $a0 $a0 2
lhu $t6 ($a0)
addiu $a0 $a0 2
lhu $t7 ($a0)
addiu $a0 $a0 2
lhu $s0 ($a0)
addiu $a0 $a0 2
lhu $s1 ($a0)
addiu $a0 $a0 2
lhu $s2 ($a0)
addiu $a0 $a0 2
lhu $s3 ($a0)
addiu $a0 $a0 2
lhu $s4 ($a0)
addiu $a0 $a0 2
lhu $s5 ($a0)
addiu $a0 $a0 2
lhu $s6 ($a0)
addiu $a0 $a0 2
lhu $s7 ($a0)

#print lines
print_lines:
li $v0 4
la $a0 topline
syscall
move $a1 $t0
move $a2 $t1
move $a3 $t2
move $t8 $t3
jal line
jal print_inbetweenline
move $a1 $t4
move $a2 $t5
move $a3 $t6
move $t8 $t7
jal line
jal print_inbetweenline 
move $a1 $s0
move $a2 $s1
move $a3 $s2
move $t8 $s3
jal line
jal print_inbetweenline
move $a1 $s4
move $a2 $s5
move $a3 $s6
move $t8 $s7
jal line
li $v0 4
la $a0 bottomline
syscall
jal print_newline
jal print_newline
# stop program
jr $ra 

# print integers
line:
li $v0 4
la $a0 wall
syscall 
move $k1 $a1

# = check length and print space with this length =
li $k0 10
bgtu $k0 $k1 four_space1
li $k0 100
bgtu $k0 $k1 three_space1
li $k0 1000
bgtu $k0 $k1 two_space1
b one_space1
one_space1:
li $v0 4
la $a0 space_one
syscall 
b end1
two_space1:
li $v0 4
la $a0 space_two
syscall 
b end1
three_space1:
li $v0 4
la $a0 space_three
syscall 
b end1
four_space1:
li $v0 4
la $a0 space_four
syscall 
b end1
end1:
# = END check length and print space with this length =

li $v0 1
move $a0 $a1
syscall
li $v0 4
la $a0 space_one
syscall 
#
li $v0 4
la $a0 wall
syscall 
move $k1 $a2

# = check length and print space with this length =
li $k0 10
bgtu $k0 $k1 four_space2
li $k0 100
bgtu $k0 $k1 three_space2
li $k0 1000
bgtu $k0 $k1 two_space2
b one_space2
one_space2:
li $v0 4
la $a0 space_one
syscall 
b end2
two_space2:
li $v0 4
la $a0 space_two
syscall 
b end2
three_space2:
li $v0 4
la $a0 space_three
syscall 
b end2
four_space2:
li $v0 4
la $a0 space_four
syscall 
b end2
end2:
# = END check length and print space with this length =

li $v0 1
move $a0 $a2
syscall
li $v0 4
la $a0 space_one
syscall 
#
li $v0 4
la $a0 wall
syscall 
move $k1 $a3

# = check length and print space with this length =
li $k0 10
bgtu $k0 $k1 four_space3
li $k0 100
bgtu $k0 $k1 three_space3
li $k0 1000
bgtu $k0 $k1 two_space3
b one_space3
one_space3:
li $v0 4
la $a0 space_one
syscall 
b end3
two_space3:
li $v0 4
la $a0 space_two
syscall 
b end3
three_space3:
li $v0 4
la $a0 space_three
syscall 
b end3
four_space3:
li $v0 4
la $a0 space_four
syscall 
b end3
end3:
# = END check length and print space with this length =

li $v0 1
move $a0 $a3
syscall
li $v0 4
la $a0 space_one
syscall 
#
li $v0 4
la $a0 wall
syscall 
move $k1 $t8

# = check length and print space with this length =
li $k0 10
bgtu $k0 $k1 four_space4
li $k0 100
bgtu $k0 $k1 three_space4
li $k0 1000
bgtu $k0 $k1 two_space4
b one_space4
one_space4:
li $v0 4
la $a0 space_one
syscall 
b end4
two_space4:
li $v0 4
la $a0 space_two
syscall 
b end4
three_space4:
li $v0 4
la $a0 space_three
syscall 
b end4
four_space4:
li $v0 4
la $a0 space_four
syscall 
b end4
end4:
# = END check length and print space with this length =

li $v0 1
move $a0 $t8
syscall
li $v0 4
la $a0 space_one
syscall 
li $v0 4
la $a0 wall
syscall 
jr $ra

print_newline:
li $v0 4
la $a0 newline
syscall
jr $ra

print_inbetweenline:
li $v0 4
la $a0 inbetweenline
syscall
jr $ra



