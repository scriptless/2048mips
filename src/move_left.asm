.text
	.globl move_left
	
#
#	$a0 buffer address
#	$a1 buffer length
#
#	|----|----|----|----|		|----|----|----|----|	
#	|  0 |  2 |  0 |  4 |	=> 	|  2 |  4 |  0 |  0 |
#	|----|----|----|----|		|----|----|----|----|	
#
	
move_left:
addiu $sp $sp -12
sw $ra ($sp)

init:
sw $a0 4($sp)
sw $a1 8($sp)

loop:
li $v0 0 # vo = 0
jal move_one
# load new addresses
lw $a0 4($sp)
lw $a1 8($sp)
# check if move_one returned 0 if so -> end
beqz $v0 end
b loop

end:
lw $ra ($sp)
addiu $sp $sp 12
jr $ra
