.text
	.globl complete_move
	

#
#	$a0 buffer address
#	$a1 buffer length
#
#	|----|----|----|----|		|----|----|----|----|
#	|  2 |  2 |  0 |  4 |  => 	|  4 |  4 |  0 |  0 |
#	|----|----|----|----|		|----|----|----|----|


complete_move:
addiu $sp $sp -12
sw $ra ($sp)
sw $a0 4($sp)
sw $a1 8($sp)

jal move_left
jal merge
lw $a0 4($sp)
lw $a1 8($sp)

end:
lw $ra ($sp)
addiu $sp $sp 12
jr $ra
