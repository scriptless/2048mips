.text
	.globl check


#
#	$a0 board address
#	$a1 board length
#
#	$v0 == 1 iff 2048 found
#

check:
li $t0 2048            		# save compare variable to t0
b check_loop           		# begin to loop through array
 
loop:
lhu $t1 ($a0)          		# load number from field address
beq $t1 $t0 found      		# check if number is 2048 -> found
addiu $a0 $a0 2        		# add 2 to field address for next field
addiu $a1 $a1 -1       		# loop counter -1 to check length

check_loop:
bgtz $a1 loop          		# is loop counter = 0 (array end) then continue

not_found:             		# -> not found
li $v0 0
jr $ra

found:
li $v0 1
jr $ra