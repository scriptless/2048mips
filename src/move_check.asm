.text
	.globl move_check
#
#	$a0 buffer address
#	$a1 buffer length
#
#   $v0 == 1 iff left move possible and would change something
#            else 0
#

# init program
move_check:
beqz $a1 move_not_possible	# checken ob feld existiert
li $t7 1			# set t7 to 1
b check_loop

# === begin loop ===
check_field:
lw $t3 ($a0)
lhu $t1 ($t3)                   # load field in t1
addiu $a0 $a0 4                 # get field+1
lw $t4 ($a0)
lhu $t2 ($t4)                   # load field+1 in t2
addiu $a1 $a1 -1
beqz $t1 field_one_empty
b field_one_value

field_one_empty:
beqz $t2 fields_same_empty
b fields_different_empty

field_one_value:
beq $t1 $t2 fields_same_value
b fields_different

fields_same_empty:
# no possible move - loop continue
b check_loop

fields_same_value:
# possible move - end loop
b move_possible

fields_different:
# no possible move - loop continue
b check_loop

fields_different_empty:
# possible move - end loop
b move_possible

check_loop:
bne $a1 $t7 check_field
b move_not_possible

# === end loop ===

move_possible:
li $v0 1
jr $ra

move_not_possible:
li $v0 0
jr $ra
