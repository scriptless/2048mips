.data
test:
	.asciiz "lol actually worked"
failed:
	.asciiz "\nfailed du opfer"
.text 
	.globl move_one
	
#
#	$a0 buffer address
#	$a1 buffer length
#
#	|----|----|----|----|----|		|----|----|----|----|----|	
#	|  2 |  0 |  2 |  0 |  4 |	=> 	|  2 |  2 |  0 |  4 |  0 |
#	|----|----|----|----|----|		|----|----|----|----|----|
#
#	$v0 1 iff something changed else 0 
move_one:
li $t7 0			# status variable 0=notchanged 1=changed

check_loop_move:
bgtz $a1 loop_move		# continue loop while buffer length is not null
b end_check

loop_move:
addiu $a1 $a1 -1    		# loop counter -1 to check length
beqz $a1 end_check		# go to end_check if length is 0
lw $t2 ($a0)
lhu $t0 ($t2)
beqz $t0 move_next_field_left	# checks if field has number 0 if so, then move next one left
#addiu $a1 $a1 -1       	# loop counter -1 to check length
addiu $a0 $a0 4
b check_loop_move

move_next_field_left:
addiu $a0 $a0 4			# get next field (!check if exist!)
lw $t3 ($a0)
lh $t1 ($t3)			# load number from field+1
beqz $t1 loop_move
li $t7 1			# sets s0 to 1 to signal that something changed
sh $zero ($t3)			# set 0 to field+1 as it moved one left
sh $t1 ($t2)			# set number to original value
b check_loop_move

end_check:
bnez $t7 end_successful		# checks if something changed s0 = 1 => yes s0 = 0 => no
b end_failed

end_failed:
li $v0 0
jr $ra

end_successful:
li $v0 1
jr $ra
